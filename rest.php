<?php

/* Exit if accessed directly. */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

const DMC_REST_API_PREFIX = 'api/v1';

/***
 * @todo Implement lazy-loading here...
 */

$resources = [
	'search',
	'user'
];

foreach ( $resources as $resource ) {
	DMC\Rest\Controller_Factory::get_controller_for_resource( $resource );
}
