<?php

namespace DMC\Rest;

use ReflectionClass;
use ReflectionMethod;

/**
 * Base class for new REST controllers -- inherit from this class to make a new
 * resource-specific REST controller. You can register a public method
 * to be an endpoint by declaring it with the following signature:
 *
 *    #[Route( <ROUTE>, <HTTP METHOD> )]
 *    public function get_whatever_resource_name( Request $request ): Response
 *
 * Then add the resource name (must be singular) to the $resources allay in
 * ig-lib/rest/rest.php to have the controller factory instantiate it.
 */
abstract class Controller_Base implements IRest_Controller
{

	/**
	 * Constructor...
	 */
	public function __construct()
	{
		$reflection = new ReflectionClass( $this );

		foreach ( $reflection->getMethods( ReflectionMethod::IS_PUBLIC ) as $method ) {
			$method_name = $method->getName();

			foreach ( $method->getAttributes() as $attribute ) {
				$name = $attribute->getName();

				if ( __NAMESPACE__ . '\Attributes\Route' === $name ) {
					$args = $attribute->getArguments();
					// [0] => route, [1] => rest method
					$this->register( $args[0], $args[1], $method_name );
				}
			}
		}
	}


	/**
	 * @param string $route
	 * @param string $method
	 * @param string $callback
	 */
	protected function register( string $route, string $method, string $callback )
	{
		$namespace = defined( 'DMC_REST_API_PREFIX' ) ? DMC_REST_API_PREFIX : 'api/v1';

		add_action(
			'rest_api_init',
			function () use ( $namespace, $callback, $route, $method ) {
				return register_rest_route(
					$namespace,
					$route, 
					[
						'methods'  => $method,
						'callback' => [ $this, $callback ],
					]
				);
			}
		);
	}

}
