<?php

namespace DMC\Rest;

class Controller_Factory
{

	/**
	 * Factory method...
	 *
	 * @param string $resource
	 * @return Controller_Base
	 */
	public static function get_controller_for_resource( string $resource ): Controller_Base
	{
		$namespace  =  __NAMESPACE__ . '\Controllers\\';
		$controller = ucwords( str_replace( '-', ' ', $resource ) ) . '_Controller';
		$controller = $namespace . str_replace( ' ', '_', $controller );

		return new $controller;
	}

}
