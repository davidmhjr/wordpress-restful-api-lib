<?php

namespace DMC\Rest\Controllers;

use DMC\Queries\Terms\Select\Tags_For_Taxonomy_Query;
use DMC\Queries\Terms\Select\Term_Meta_From_Name_Query;
use DMC\Rest\Attributes\Route;
use DMC\Rest\Controller_Base;
use WP_REST_Response as Response;
use WP_REST_Request as Request;
use WP_REST_Server as Server;

class Search_Controller extends Controller_Base
{

	/**
	 * @param Request $request
	 * @return mixed
	 */
	#[Route( 'search-terms', Server::READABLE )]
	public function search_terms( Request $request ): mixed
	{
		$query = str_replace( ['"'], '', $request->get_param( 'q' ) );

		// Uses a "query" class not included here...
		return ( new Tags_For_Taxonomy_Query() )->execute( [
			'match_str' => $query,
			'taxonomy'  => 'search_term',
			'limit'     => 5,
		] );
	}

	/**
	 * @param Request $request
	 * @return mixed
	 */
	#[Route( 'search-terms/by-name', Server::READABLE )]
	public function search_terms_exact_match( Request $request ): mixed
	{
		$query = str_replace( ['"', '/'], '', $request->get_param( 'q' ) );

		// Uses a "query" class not included here...
		return ( new Term_Meta_From_Name_Query() )->execute( [
			'name' => $query,
			'taxonomy'  => 'search_term',
		] );
	}

}
