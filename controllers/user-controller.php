<?php

namespace DMC\Rest\Controllers;

use DMC\Rest\Attributes\Route;
use DMC\Rest\Controller_Base;
use WP_REST_Response as Response;
use WP_REST_Request as Request;
use WP_REST_Server as Server;

class User_Controller extends Controller_Base
{

	/**
	 * @param Request $request
	 * @return mixed
	 */
	#[Route( '/users/(?P<id>[\d]+)', Server::CREATABLE )]
	public function user_exists( Request $request ): Response
	{
		// Put something useful here...
		return new Response( 200, 'success' );
	}

}
